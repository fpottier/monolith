(******************************************************************************)
(*                                                                            *)
(*                                  Monolith                                  *)
(*                                                                            *)
(*                              François Pottier                              *)
(*                                                                            *)
(*  Copyright Inria. All rights reserved. This file is distributed under the  *)
(*  terms of the GNU Lesser General Public License as published by the Free   *)
(*  Software Foundation, either version 3 of the License, or (at your         *)
(*  option) any later version, as described in the file LICENSE.              *)
(*                                                                            *)
(******************************************************************************)

open Spec
open Support

(* This file offers ready-made functions that help deal with tuples. *)

let constructible_triple spec1 spec2 spec3 =
  map_outof
    Tuple.unnest3
    Tuple.Unnest3.code
    (spec1 *** (spec2 *** spec3))

let deconstructible_triple spec1 spec2 spec3 =
  map_into
    Tuple.nest3
    Tuple.Nest3.code
    (spec1 *** (spec2 *** spec3))

let triple spec1 spec2 spec3 =
  ifpol
    (constructible_triple spec1 spec2 spec3)
    (deconstructible_triple spec1 spec2 spec3)
